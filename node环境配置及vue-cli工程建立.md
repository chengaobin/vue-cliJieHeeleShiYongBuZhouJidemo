## 安装node
下载:官网下载:http://nodejs.cn/
点击下一步安装即可.一般会自动配置环境变量
如果没有配置好环境变量,找到nodejs的安装目录,配置到环境变量即可.
## 安装淘宝镜像

  1.国内直接使用 npm 的官方镜像是非常慢的，这里推荐使用淘宝 NPM 镜像

  npm install -g cnpm --registry=https://registry.npm.taobao.org

  (npm config set registry https://registry.npm.taobao.org)

  2.这样就可以使用 cnpm 命令来安装模块了：

  cnpm install [name]

## 使用vue-cli建立vue单页应用.
### 安装webpack
cnpm install webpack -g(-g代表全局安装)

### 安装vue脚手架
npm install vue-cli -g

在硬盘上找到一个安装项目的目录

cd [目录]

### vue可以从网上down模板,可以根据模板来创建项目

vue init webpack 工程名字<工程名字不能用中文>(这是创建vue 2.0的项目,推荐使用).

在步骤中会有一系列初始化设置:

Target directory exists. Continue? (Y/n)直接回车默认(然后会下载 vue2.0模板，这里可能需要连代理)

Project name (vue-test)填写项目名称,或者直接回车

Project description (A Vue.js project) 填写项目描述或者直接回车

Author 写你自己的名字

*注意* 填写初始化信息时,Eslint验收,选择N,不然你写的代码都需要遵守eslint语法规范,当然,你会的话可以选择Y

### cd 进入你自己的项目

    cd [project name]

### 安装项目依赖

*注意* 一定要从官方仓库安装，npm 服务器在国外所以这一步安装速度会很慢。不要从国内镜像cnpm安装(会导致后面缺了很多依赖库)

### 安装 vue 网络请求模块vue-resource

npm install

### 启动项目

npm run dev

界面如图:
![项目启动首页](https://gitee.com/uploads/images/2017/1113/104647_c3cdb125_1235555.png "demo.png")



## 安装可能出现的问题

以下坑可能由于 vue2.0 导致其他相关编译打包工具没更新导致的

【重点】后来发现这些坑是由于 npm 不是最新的版本3.10.2， 用 npm 3.9.5就会出现以下坑
解决办法: 请运行以下命令
npm update -g

### 报错
Error: Cannot find module 'opn'

Error: Cannot find module 'webpack-dev-middleware'

Error: Cannot find module 'express'

Error: Cannot find module 'compression'

Error: Cannot find module 'sockjs'

Error: Cannot find module 'spdy'

Error: Cannot find module 'http-proxy-middleware'

Error: Cannot find module 'serve-index'

如果你用的是老版本的 vue-cli 还可能报其他错误，需要更新一下 vue-cli

npm update vue-cli

安装一下这个依赖到工程开发环境

cnpm install opn --save-dev

cnpm install webpack-dev-middleware --save-dev

cnpm install express --save-dev

cnpm install compression --save-dev

cnpm install sockjs --save-dev

cnpm install spdy --save-dev

cnpm install http-proxy-middleware --save-dev

cnpm install serve-index --save-dev

cnpm install connect-history-api-fallback --save-dev

### 解决办法概述

遇到

Module build failed: Error: Cannot find module '模块名'

那就安装
cnpm install 模块名 --save-dev(关于环境的，表现为npm run dev 启动不了)

cnpm install 模块名 --save(关于项目的，比如main.js，表现为npm run dev 成功之后控制台报错)

比如escape-string-regexp、strip-ansi、has-ansi、is-finite、emojis-list








