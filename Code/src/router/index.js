import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import FirstPage from '@/components/firstPage'
import secondPage from '@/components/secondPage'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'HelloWorld',
        component: HelloWorld,
        children: [{
            path: '/firstPage',
            name: FirstPage,
            component: FirstPage
        }, {
            path: '/sacondPage',
            name: secondPage,
            component: secondPage
        }]
    }]
})