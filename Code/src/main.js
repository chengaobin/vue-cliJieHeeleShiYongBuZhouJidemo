// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.directive('drag', function(el) {
    el.onmousedown = function(e) {
        el.style.width = "100%";
        el.style.cursor = "move ";
        //获取鼠标点击处分别与div左边和上边的距离：鼠标位置-div位置
        var disX = e.clientX - el.offsetLeft;
        var disY = e.clientY - el.offsetTop;
        // console.log(disX,disY);

        //包含在onmousedown里面，表示点击后才移动，为防止鼠标移出div，使用document.onmousemove
        document.onmousemove = function(e) {
            //获取移动后div的位置：鼠标位置-disX/disY
            var l = e.clientX - disX;
            var t = e.clientY - disY;
            el.style.left = l + 'px';
            el.style.top = t + 'px';
        }

        //停止移动
        document.onmouseup = function(e) {
            // el.style.cursor = "auto ";
            document.onmousemove = null;
            document.onmouseup = null;
        }

    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})