## 工程目录(IDE自己选择,vs code,webstorm,sublime3...本人使用vscode)

![vs code界面](https://gitee.com/uploads/images/2017/1113/111214_7c246799_1235555.png "ide.png")

### 目录介绍
![输入图片说明](https://gitee.com/uploads/images/2017/1113/111652_f9975a69_1235555.png "22222.png")
1.node_modules

此目录是项目的依赖文件,例如安装了element-ui,jq等库

2.index.html

工程的入口文件

3.package.json

项目的描述文件

![输入图片说明](https://gitee.com/uploads/images/2017/1113/112110_c086f354_1235555.png "在这里输入图片标题")

包括项目的名称,描述,项目的依赖,项目启动对浏览器的要求等.

4.src

项目的主要开发目录

4.1 assets(默认)

    静态资源文件夹

4.2 components(默认没有,需要自己建)

    vue单页项目的组件文件夹,vue-cli是以一个个.vue文件组合而成.

4.3 constant(默认没有,需要自己建)

    公共方法文件一般是js文件.写一些公共的方法,例如cookie操作等等.

4.4 router(默认没有,需要自己建)

    路由文件,vue单页面项目页面的跳转是根据路由配置来进行跳转(router-view标签跳转),所以需要配置不同的路由跳转的组件页面.

    